import React, { useRef, useState } from "react";
import { useIntl } from "gatsby-plugin-intl";

import "../styles/pages/Terms.scss";

import Layout from "../components/Layout.jsx";
import Seo from "../components/Seo";

export default function Terms() {
  const intl = useIntl();
  const [tab, setTab] = useState(0);
  const contentRef = useRef(null);

  const tab0 = tab === 0;
  const tab1 = tab === 1;
  const tab2 = tab === 2;
  const tab3 = tab === 3;
  const tab4 = tab === 4;
  const tab5 = tab === 5;

  const handleTab = (tabSelected) => () => {
    setTab(tabSelected);
    contentRef.current.firstElementChild.scrollTo(0, 0);
    contentRef.current.scrollIntoView({ behavior: "smooth" });
  };

  return (
    <Layout>
      <Seo title={intl.formatMessage({ id: "terms-title" })} />
      <div className="terms">
        <section className="terms__tabs">
          <button onClick={handleTab(0)} type="button" className="terms__tab">
            <p className="terms__tab-text">{intl.formatMessage({ id: "terms-tab1" })}</p>
          </button>
          <button onClick={handleTab(1)} type="button" className="terms__tab">
            <p className="terms__tab-text">{intl.formatMessage({ id: "terms-tab2" })}</p>
          </button>
          <button onClick={handleTab(2)} type="button" className="terms__tab">
            <p className="terms__tab-text">{intl.formatMessage({ id: "terms-tab3" })}</p>
          </button>
          <button onClick={handleTab(3)} type="button" className="terms__tab">
            <p className="terms__tab-text">{intl.formatMessage({ id: "terms-tab4" })}</p>
          </button>
          <button onClick={handleTab(4)} type="button" className="terms__tab">
            <p className="terms__tab-text">{intl.formatMessage({ id: "terms-tab5" })}</p>
          </button>
          <button onClick={handleTab(5)} type="button" className="terms__tab">
            <p className="terms__tab-text">{intl.formatMessage({ id: "terms-tab6" })}</p>
          </button>
        </section>
        <section ref={contentRef} className="terms__content">
          {
            tab0 ? (
              <>
                <h1 className="terms__content-title">{intl.formatMessage({ id: "terms-title1" })}</h1>
                <p className="terms__content-text">
                  {intl.formatMessage({ id: "terms-text1" })}
                </p>
              </>
            ) : null
          }
          {
            tab1 ? (
              <>
                <h1 className="terms__content-title">{intl.formatMessage({ id: "terms-tab2" })}</h1>
                <p className="terms__content-text">
                  {intl.formatMessage({ id: "terms-text2" })}
                </p>
              </>
            ) : null
          }
          {
            tab2 ? (
              <>
                <h1 className="terms__content-title">{intl.formatMessage({ id: "terms-tab3" })}</h1>
                <p className="terms__content-text">
                  {intl.formatMessage({ id: "terms-text3" })}
                </p>
              </>
            ) : null
          }
          {
            tab3 ? (
              <>
                <h1 className="terms__content-title">{intl.formatMessage({ id: "terms-tab4" })}</h1>
                <p className="terms__content-text">
                  {intl.formatMessage({ id: "terms-text4" })}
                </p>
              </>
            ) : null
          }
          {
            tab4 ? (
              <>
                <h1 className="terms__content-title">{intl.formatMessage({ id: "terms-title2" })}</h1>
                <p className="terms__content-text">
                  {intl.formatMessage({ id: "terms-text5" })}
                </p>
              </>
            ) : null
          }
          {
            tab5 ? (
              <>
                <h1 className="terms__content-title">{intl.formatMessage({ id: "terms-tab6" })}</h1>
                <p className="terms__content-text">
                  {intl.formatMessage({ id: "terms-text6" })}
                </p>
              </>
            ) : null
          }
        </section>
      </div>
    </Layout>
  );
}
